(function() {
    var menu = document.querySelector('header .main-nav');
    var mobileButton = document.querySelector('header .mobile-menu');
    var hiddenClassName = 'mobile-hidden';

    if (!menu || !mobileButton) {
        return;
    }

    mobileButton.addEventListener('click', function (event) {
        event.preventDefault();

        if (menu.classList.contains(hiddenClassName)) {
            menu.classList.remove(hiddenClassName);
        } else {
            menu.classList.add(hiddenClassName);
        }
    }, false);
})();
